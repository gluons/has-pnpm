# has-pnpm
[![NPM](https://img.shields.io/npm/l/has-pnpm.svg?style=flat-square)](./LICENSE)
[![npm](https://img.shields.io/npm/v/has-pnpm.svg?style=flat-square)](https://www.npmjs.com/package/has-pnpm)
[![npm](https://img.shields.io/npm/dt/has-pnpm.svg?style=flat-square)](https://www.npmjs.com/package/has-pnpm)
![npm type definitions](https://img.shields.io/npm/types/has-pnpm.svg?style=flat-square)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/gluons/has-pnpm.svg?style=flat-square)](https://gitlab.com/gluons/has-pnpm/pipelines)

Check if a project is using [pnpm](https://pnpm.js.org/).

> It'll check whether `shrinkwrap.yaml` or `pnpm-lock.yaml` exists in current working directory.

## Installation

```bash
npm install has-pnpm
# or
yarn add has-pnpm
# or
pnpm install has-pnpm
```

## CLI

You can also install as global package to use CLI.

```bash
npm install -g has-pnpm
# or
yarn global add has-pnpm
```

```
has-pnpm

Check if a project is using pnpm.

Options:
  --help, -h     Show help                                             [boolean]
  --version, -v  Show version number                                   [boolean]
```

## Usage

```
├── a
│   ├── package.json
│   └── package-lock.json
├── b
│   ├── package.json
│   └── shrinkwrap.yaml
└── c
    ├── package.json
    └── pnpm-lock.yaml
```

```js
const hasPNPM = require('has-pnpm');

hasPNPM('a'); // => false
hasPNPM('b'); // => true
hasPNPM('c'); // => true
```

## API

### hasPNPM(cwd)
**Return:** `boolean`

Return result of checking pnpm usage.

#### cwd
**Type:** `string`  
**Default:** `process.cwd()`

Current working directory.

## Related

- [has-yarn](https://github.com/sindresorhus/has-yarn) - Check if a project is using **Yarn**.
- [caniuse-pnpm](https://gitlab.com/gluons/caniuse-pnpm) - Check if **pnpm** is available.
