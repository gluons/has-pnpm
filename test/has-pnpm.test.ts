import { resolve } from 'path';

import hasPNPM from '../dist';

const legacyPath = resolve(__dirname, './fixtures/legacy/');
const v3Path = resolve(__dirname, './fixtures/v3/');
const npmPath = resolve(__dirname, './fixtures/npm/');

describe('Legacy', () => {
	it('should return `true` for directory that use legacy pnpm', () => {
		expect(hasPNPM(legacyPath)).toBe(true);
	});
});

describe('Version 3', () => {
	it('should return `true` for directory that use v3+ pnpm', () => {
		expect(hasPNPM(v3Path)).toBe(true);
	});
});

describe('Without pnpm', () => {
	it("should return `false` for directory that don't use pnpm", () => {
		expect(hasPNPM(npmPath)).toBe(false);
	});
});
