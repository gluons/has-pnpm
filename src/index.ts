import { existsSync } from 'fs';
import { resolve } from 'path';

const pnpmLockFiles = ['shrinkwrap.yaml', 'pnpm-lock.yaml'];

/**
 * Check if a project is using pnpm.
 *
 * @param {string} [cwd=process.cwd()] Current working directory
 * @returns {boolean}
 */
function hasPNPM(cwd: string = process.cwd()): boolean {
	return pnpmLockFiles.some(lockFile => {
		const lockFilePath = resolve(cwd, lockFile);

		return existsSync(lockFilePath);
	});
}

export = hasPNPM;
