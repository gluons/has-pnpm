#!/usr/bin/env node

import chalk from 'chalk';
import { cross, tick } from 'figures';
import { EOL } from 'os';
import yargs from 'yargs';

import hasPNPM from './index';

const { cyan, green, red } = chalk;
const pnpm = cyan('pnpm');

// tslint:disable:no-unused-expression

yargs
	.scriptName(cyan('has-pnpm'))
	.help()
	.version()
	.alias('help', 'h')
	.alias('version', 'v')
	.usage('$0', 'Check if a project is using pnpm.')
	.argv;

// tslint:enable:no-unused-expression

if (hasPNPM(process.cwd())) {
	process.stdout.write(
		green(`${tick} Current project is using ${pnpm}.${EOL}`)
	);
} else {
	process.stdout.write(
		red(`${cross} Current project does not use ${pnpm}.${EOL}`)
	);
}
